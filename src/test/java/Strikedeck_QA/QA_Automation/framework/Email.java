package Strikedeck_QA.QA_Automation.framework;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Email {

	public static void main(String[] args) throws IOException {
		//run this for testing purposes
		AccountObj.readFile();
		//email(1, "");
		
	}
	
	public static void email(int customerIndex, String body, int numberFailedCases) {
		String from = "strikedeck_QA@strikedeck.com"; 
		String recipient = AccountObj.accountList.get(customerIndex).crm;

		String customerName = AccountObj.accountList.get(customerIndex).customerName;
		String subject = "Strikedeck: "+ customerName +" Report";

		String heading =
		"<p>Hey there!</p>"
		+"<br>"
		+"<p>Here's the summary of <b>"+ customerName +"</b> test report: <br>Number of failed test cases: <b>" + numberFailedCases +"</b></p>"
		+"<table>"
		+"<tr>"
				+"<th  ><b>Test Case</b></td>"
				+"<th  ><b>Result</b></td>"
	   + "</tr>"; 

	   String style = 
	   "<html><head>"
	   +"<style>"
	   +"h2 {font-family: Arial, Helvetica, sans-serif; font-size: 1.5em}"
	   +"table{font-family: Arial, Helvetica, sans-serif; font-size: small; border-collapse: collapse; width: 80%}"
	   +"td, th{border: 1px solid; padding: 5px; text-align: left}"
	   +"td {font-weight: normal}"
	   +"</style>"
	   +"</hedd>";

	   String html = style + heading + body + "</table>";
		
		sendFromGMail(from, recipient, subject, html);
	}

	private static void sendFromGMail(String from, String recipient, String subject ,String body) {
		String host = "74.125.20.26"; 
		
		Properties props = System.getProperties();
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.port", "25");
		//props.put("mail.debug", "true");

		Session session = Session.getDefaultInstance(props);

		try {
			// Create a default Message object.
			MimeMessage message = new MimeMessage(session);
			

			// Set from email
			message.setFrom(new InternetAddress(from));

			// Set recipient
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(recipient));

			// Set email subject
			message.setSubject(subject);

			// Set body message
			message.setContent(body, "text/html");

			// Send message
			Transport.send(message);
			System.out.println("Message sent");
		} 
		catch (MessagingException mex) {
			mex.printStackTrace();
		}
	}

	public static String tableContent(List<List<String>> completeTestResult){
		String html = "";

	  // System.out.println(completeTestResult.size());
	   //System.out.println(completeTestResult.get(0));

	   for (int i = 0; i < completeTestResult.size(); i++){
		   if (completeTestResult.get(i).get(1).equals("Fail")){
			html += 
			"<tr style='background-color: #f78a8a'>"
			+"<td >"+ completeTestResult.get(i).get(0) +"</td>"
			+"<td >"+ completeTestResult.get(i).get(1) +"</td>"
			+"</tr>";
		   }
		   else{
			html += 
			"<tr>"
			+"<td>"+ completeTestResult.get(i).get(0) +"</td>"
			+"<td>"+ completeTestResult.get(i).get(1) +"</td>"
			+"</tr>";
		   }
	}


	//    for (int i = 0; i < completeTestResult.size(); i++){
	// 	   html += 
	// 	   "<tr>"
	// 	   +"<td>"+ completeTestResult.get(i).get(0) +"</td>"
	// 	   +"<td>"+ completeTestResult.get(i).get(1) +"</td>"
	// 	   +"</tr>";
	//    }

	   return html;
	}

}
