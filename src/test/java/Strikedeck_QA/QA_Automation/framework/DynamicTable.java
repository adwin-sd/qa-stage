package Strikedeck_QA.QA_Automation.framework;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class DynamicTable {

	WebDriver driver;
	public String podID;
	// Some tables are not located in a pod, but in those cases, they are the only
	// table in the page
	public boolean inPod;
	// XPath for the table name
	public String tableName = "//div[@id='%s']//div[contains(@class, 'card-header')]//h4";
	// XPath for the name for each column
	String tableHeader = "//div[@class='fixed-table-body']//table" + "//th/div[contains(@class, 'th-inner')]";
	// XPath for pagination info which contains text for example: "Showing 1 to 20
	// of 41 rows"
	String paginationInfo = "//span[@class='pagination-info']";
	// XPath of element that is shown if there is no data in table
	String noRecords = "//div[@class='fixed-table-body']//tr[@class = 'no-records-found']";
	// XPath of the next page button for table
	String nextPage = "//div[@class ='fixed-table-pagination']//li[@class = 'page-item page-next']//a";

	// The table is identified by the Pod ID. Hence it is important to detect what
	// is the pod ID in the page
	public DynamicTable(WebDriver driver, String podID) {
		this.driver = driver;
		this.podID = podID;
		this.inPod = true;
		this.tableName = podCheckXpath(this.tableName);
		this.tableHeader = podCheckXpath(this.tableHeader);
		this.paginationInfo = podCheckXpath(this.paginationInfo);
		this.noRecords = podCheckXpath(this.noRecords);
		this.nextPage = podCheckXpath(this.nextPage);
	}

	public DynamicTable(WebDriver driver) {
		this.driver = driver;
		this.inPod = false;

	}

	// Check if dynamicTable is in a pod
	// If it is, then the xpath will be converted
	public String podCheckXpath(String xPath) {
		String inPodXpath = String.format("//div[@id= '%1$s']", podID);
		if (inPod) {
			xPath = inPodXpath + xPath;
		}
		return xPath;
	}

	// Get Table name
	public String getTableName() {
		return driver.findElement(By.xpath(this.tableName)).getAttribute("textContent");
	}

	public List<String> retrieveFieldTitles() {
		List<WebElement> titleList = driver.findElements(By.xpath(this.tableHeader));
		List<String> titleNameList = new ArrayList<String>();
		for (WebElement i : titleList) {
			titleNameList.add(i.getAttribute("textContent"));
		}

		return titleNameList;
	}

	// Retrieves the total number of data in the table
	public int totalNoOfRows() {
		System.out.println("Getting total number of row......");

		String text = driver.findElement(By.xpath(this.paginationInfo)).getAttribute("textContent");
		String[] separated = text.split(" ");
		String totalNoStr = (String) Array.get(separated, 5);
		int totalNo = Integer.parseInt(totalNoStr);
		return totalNo;
	}

	// Check if table has any data
	public boolean checkDataExist() {
		List<WebElement> check = this.driver.findElements(By.xpath(this.noRecords));
		int size = check.size();
		if (size == 0) {
			System.out.println("Data exist in Table");
			return true;
		} else {
			System.out.println("Data does not exist in Table");
			return false;
		}
	}

	// Does not check if columnName exist
	public int retrieveColumnPos(String columnName) {
		// Using just xpath to retrieve the position
		String titleXpath = "//div[@class='fixed-table-body']//table/thead/tr/th/div[text()='%s']/parent::*/preceding-sibling::*";
		titleXpath = String.format(titleXpath, columnName);
		titleXpath = podCheckXpath(titleXpath);

		System.out.println("Retrieving Column position......");
		int columnPos = driver.findElements(By.xpath(titleXpath)).size() + 1;
		return columnPos;
	}

	// Click on the > button in the pagination, to go to the next table page
	// Check if the page is the last page of the table. Returns false if the current
	// page is the last page
	public boolean nextTablePage() throws InterruptedException {
		// Xpath to check if the table page is the last one
		String pageCheckXpath = "//div[@class ='fixed-table-pagination']//li[@class = 'page-item active' or @class = 'page-item'][last()]";
		pageCheckXpath = podCheckXpath(pageCheckXpath);

		WebElement pageCheck = driver.findElement(By.xpath(pageCheckXpath));
		if (pageCheck.getAttribute("class").equals("page-item active")) {
			return false;
		} else {
			driver.findElement(By.xpath(nextPage)).click();
			return true;
		}

	}

	// Suggested to filter, before retrieving
	public List<String> retrieveColumnData(String columnName) throws InterruptedException {
		int columnPos = retrieveColumnPos(columnName);
		String columnDataXpath = String.format("//div[@class='fixed-table-body']//table//tbody//tr//td[%s]", columnPos);
		columnDataXpath = podCheckXpath(columnDataXpath);
		int counter = 0;
		List<WebElement> elementList = new ArrayList<WebElement>();
		List<String> returnList = new ArrayList<String>();
		@SuppressWarnings("unused")
		int aCounter = 0;

		do {
//			try {
			TimeUnit.SECONDS.sleep(3);
			counter += 1;
			System.out.println("retrieveColumn: " + counter);
			elementList.addAll(driver.findElements(By.xpath(columnDataXpath)));
			System.out.println("Number of row " + elementList.size());
			for (WebElement i : elementList) {
				aCounter += 1;
				returnList.add(i.getAttribute("textContent"));

			}
			elementList.clear();

//			} catch (org.openqa.selenium.StaleElementReferenceException ex) {
//				TimeUnit.SECONDS.sleep(3);
//				counter += 1;
//				System.out.println("retrieveColumn: " + counter);
//				elementList.addAll(driver.findElements(By.xpath(columnDataXpath)));
//				System.out.println("Number of row " + elementList.size());
//				for (WebElement i : elementList) {
//					aCounter += 1;
//					returnList.add(i.getAttribute("textContent"));
//				}
//				elementList.clear();
//			}
		} while (nextTablePage());

		return returnList;
	}

	// filter
	public void filter(String columnName, String filterData) {
		String xPath = String.format("//div[@class='fixed-table-body']//table//thead/tr"
				+ "//div[contains(text(), '%s')]" + "//..//div[@class = 'filter-control']/*", columnName);

		xPath = podCheckXpath(xPath);
		WebElement wElement = driver.findElement(By.xpath(xPath));

		if (wElement.getTagName().equals("select")) {
			Select ddl = new Select(wElement);
			ddl.selectByVisibleText(filterData);
		} else {
			wElement.sendKeys(filterData);
		}
	}

	public List<Map<String, String>> retrieveSpecificRowData(String columnName, String data)
			throws InterruptedException {
		List<Map<String, String>> result = new ArrayList<Map<String, String>>();

		// xpath for retrieve all rows with the data in the column
		String xPath = "//div[@class='fixed-table-body']//table/tbody/tr//td[%1$s]//descendant-or-self::*[contains(text(), '%2$s')]//ancestor::tr/td";
		int pos = retrieveColumnPos(columnName);
		xPath = String.format(xPath, pos, data);
		List<String> titles = retrieveFieldTitles();

		List<WebElement> wList = new ArrayList<WebElement>();
		Map<String, String> map = new HashMap<String, String>();
		String key = null;
		String rData = null;
		int fieldCount = 0;

		int noOfFields = titles.size();

		do {
			TimeUnit.SECONDS.sleep(2);
			wList = driver.findElements(By.xpath(xPath));

			for (int i = 0; i < wList.size(); i++) {
				key = titles.get(fieldCount);
				rData = wList.get(i).getAttribute("textContent");
				map.put(key, rData);
				if (fieldCount < (noOfFields - 1)) {
					fieldCount += 1;
				} else {
					result.add(map);
					fieldCount = 0;
					map = new HashMap<String, String>();
				}

			}

		} while (nextTablePage());

		return result;
	}

	// Suggested to run filter method before calling this method to
	// narrow the data set to search
	// Retrieve all row data
	public List<Map<String, String>> retrieveAllRowData() throws InterruptedException {
		List<Map<String, String>> result = new ArrayList<Map<String, String>>();

		// xpath for retrieve all rows with the data in the column
		String xPath = "//div[@class='fixed-table-body']//table/tbody/tr//td";
		List<String> titles = retrieveFieldTitles();

		List<WebElement> wList = new ArrayList<WebElement>();
		Map<String, String> map = new HashMap<String, String>();
		String key = null;
		String rData = null;
		int fieldCount = 0;

		int noOfFields = titles.size();

		do {
			TimeUnit.SECONDS.sleep(2);
			wList = driver.findElements(By.xpath(xPath));

			for (int i = 0; i < wList.size(); i++) {
				key = titles.get(fieldCount);
				rData = wList.get(i).getAttribute("textContent");
				map.put(key, rData);
				if (fieldCount < (noOfFields - 1)) {
					fieldCount += 1;
				} else {
					result.add(map);
					fieldCount = 0;
					map = new HashMap<String, String>();
				}

			}

		} while (nextTablePage());

		return result;
	}

	// Goes through each row to find the specified data that is tied to the
	// specified column
	// Use for one unique column only
	public boolean checkSpecificData(String columnName, String data) throws InterruptedException {
		retrieveColumnData(columnName);

		// Using xpath to retrieve all specified data in the column.
		String xPath = String.format("//div[@class='fixed-table-body']"
				+ "//table/tbody/tr//*[self::a[text() = '%1$s'] or self::td[text() = '%1$s']]", data);
		xPath = podCheckXpath(xPath);

		if (driver.findElements(By.xpath(xPath)).size() > 0) {
			System.out.println("There are " + driver.findElements(By.xpath(xPath)).size()
					+ " opportunity with the same specified information");
			return true;
		} else {
			System.out.println("There are " + driver.findElements(By.xpath(xPath)).size()
					+ " opportunity with the same specified information");
			return false;
		}
	}

	// Click the add symbol in the table
	public void selectAddData() {
		// x-path for the Add Btn
		String xPath = "//div[@class = 'card-body']/div[contains(@class, 'card-header')]/div[@class = 'd-flex']//div[contains(@class, 'addBtn')]/a";
		xPath = podCheckXpath(xPath);
		driver.findElement(By.xpath(xPath)).click();

	}

	// Add column in the table
	// columnName is the column that is to be added
	// return false if column does not exist
	public boolean addColumn(String columnName) {
		// X-Path for the column button
		String xPath = "//div[@class='btn btn-link btn-sm p-0']";
		xPath = podCheckXpath(xPath);
		driver.findElement(By.xpath(xPath)).click();
		driver.switchTo().activeElement();
		String colXPath = String.format("//div[@id='fieldPanel']/ul/li/span[text() = '%s']/parent::*", columnName);
		List<WebElement> list = driver.findElements(By.xpath(colXPath));
		if (list.size() > 0) {
			for (WebElement i : list) {
				i.click();
			}
		} else {
			return false;
		}

		String addXPath = "//div[contains(@class,'p-4 columnConfigForm')]//div[2]//button[1]";
		WebElement i = driver.findElement(By.xpath(addXPath));
		i.click();

		return true;
	}

	// Incomplete methods

	// return false if column does not exist
	public boolean removeColumn(String columnName) {
		// X-Path for the column button
		String xPath = "//div[@class='btn btn-link btn-sm p-0']";
		xPath = podCheckXpath(xPath);
		driver.findElement(By.xpath(xPath)).click();
		driver.switchTo().activeElement();
		return false;
	}

}
