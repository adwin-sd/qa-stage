package Strikedeck_QA.QA_Automation.framework;

import java.io.FileNotFoundException;

import java.io.IOException;

//import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;

// import Strikedeck_QA.QA_Automation.AccountObj;
// import Strikedeck_QA.QA_Automation.Basics;
// import Strikedeck_QA.QA_Automation.Email;
// import Strikedeck_QA.QA_Automation.ErrorMessages;
// import Strikedeck_QA.QA_Automation.ExcelReport;

public class TestCaseStructure {
	static WebDriver driver;
	static WebDriverWait wait;
	static boolean testResult = true;
	
	//set value of variables
	//id number for error message in errorMessagesTemplate
	static int errID = 0;
	//name of test case for excel pass/fail report
	static String testCaseName = " ";
	
	/*
	 * Call when test case fails
	 * sets testResult to false to be reflected in Report
	 * sends email to CSM
	 */
	public static void error(int instanceID /* also required placeholder values */) throws FileNotFoundException {
		//get customer name for error message (should be required for all)
		String customer = AccountObj.accountList.get(instanceID).customerName;
		testResult = false;
		
		//the test has failed
		System.out.println("Test Failed. Sending error message to CSM");
		
		//values that go into place holders of errorMessage
		//string is most convenient, other value types will require modifications
		String[] fillValues = new String[] {customer /*, add, other, values */};
		
		//send an email
	}
	

	public static void main(String[] args) throws IOException, InterruptedException {
		AccountObj.readFile();
		System.setProperty("webdriver.chrome.driver", "drivers/chromedriver");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--headless", "--disable-gpu", "--window-size=1920,1200", "--ignore-certificate-errors");
		//ExcelReport.setUpExcelReport();

		for (int instanceID = 1; instanceID < AccountObj.accountList.size(); instanceID++) {

			driver = new ChromeDriver(options);

			System.out.println("Test: Verify Customer Count");

			// login
			Basics.login(driver, instanceID);
			
			//call other methods
			Basics.getTileId(driver, "Customers");
			
			//write result of instance to excel report
			//ExcelReport.writeTestCases(testCaseName, instanceID, testResult);

			testResult = true;
			
			//exit
			driver.quit();
		}
	}

}
