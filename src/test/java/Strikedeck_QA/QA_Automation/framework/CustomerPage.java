package Strikedeck_QA.QA_Automation.framework;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CustomerPage extends Page {

	public CustomerPage(WebDriver driver) {
		super(driver);
	}

	// tiles
	// Each tile has an ID
	public List<String> getTileID() {
		List<String> returnList = new ArrayList<String>();
		List<WebElement> list = driver.findElements(By.cssSelector(".px-1"));
		for (WebElement i : list) {
			returnList.add(i.getAttribute("id"));
		}

		return returnList;

	}

	public void filter(String filter, String data) {

	}

}