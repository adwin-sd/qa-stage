package Strikedeck_QA.QA_Automation.framework;

import java.io.File;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AccountObj {
	public String customerName;
	public String instance;
	public String email;
	public String password;
	public String crm;
	public List<String> dataList = new ArrayList<>();
	//list to store all customer objects
	public static List<AccountObj> accountList = new ArrayList<>();
	public static String fileName = "doc/customerDetails.csv";
	
	
	public static void readFile() throws FileNotFoundException {
		//clear list for subsequent iterations
		accountList.clear();
		
		Scanner scanner = new Scanner(new File(fileName));
		
		Scanner dataScanner = null;
		int index = 0;
		
		while (scanner.hasNextLine()) {
			dataScanner = new Scanner(scanner.nextLine());
			dataScanner.useDelimiter(",");
			AccountObj account = new AccountObj();
			
			while (dataScanner.hasNext()) {
				String data = dataScanner.next();
				
				if (index == 0)
					account.customerName = data;
				
				else if (index == 1)
					account.instance = data;
				
				else if (index == 2)
					account.email = data;
				
				else if (index == 3)
					account.password = data;
				
				else if (index == 4)
					account.crm = data;
				
				else
					account.dataList.add(data);
				
				index++;
			}
			index = 0;
			accountList.add(account);
		}
		scanner.close();
	}
	public static List<String> getHeaders() throws FileNotFoundException {
		List<String> headers = new ArrayList<>();

		Scanner scanner = new Scanner(new File(fileName));
		Scanner dataScanner = null;
		//get headers
		dataScanner = new Scanner(scanner.nextLine());
		//split headers with ","
		dataScanner.useDelimiter(",");
		
		while (dataScanner.hasNext()) {
			String data = dataScanner.next();
			headers.add(data.toLowerCase());
		}
		
		scanner.close();
		dataScanner.close();

		return headers;
	}

	public static int getDataIndex(String headingName) throws FileNotFoundException {
		List<String> headers = new ArrayList<>();
		
		int headerIndex = 0;
		
		//read file
		Scanner scanner = new Scanner(new File(fileName));
		
		Scanner dataScanner = null;
		//get headers
		dataScanner = new Scanner(scanner.nextLine());
		//split headers with ","
		dataScanner.useDelimiter(",");
		
		while (dataScanner.hasNext()) {
			String data = dataScanner.next();
			headers.add(data.toLowerCase());
		}
		
		for (int i = 0; i < headers.size(); i++) {
			if (headers.get(i).equalsIgnoreCase(headingName.toLowerCase())) {
				headerIndex = i;
				break;
			}
		}
		//return index in DataList
		scanner.close();
		dataScanner.close();
		return (headerIndex - 5);
	}
	
	public static void main(String[] args) throws FileNotFoundException {
		readFile();
		
		//testing
		int x = getDataIndex("test2");
		// System.out.println(accountList.get(1).dataList.get(x));
		System.out.println(accountList.get(2).customerName);
	}
}
