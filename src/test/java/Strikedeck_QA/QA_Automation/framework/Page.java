package Strikedeck_QA.QA_Automation.framework;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Page {
	WebDriver driver;

	public Page(WebDriver driver) {
		this.driver = driver;
	}

	// Tab

	// Get current Tab
	public String currentTabName() {
		String xPath = "//ul[@id='pills-tab']//a[contains(@class, 'active show')]";
		String returnTabName = driver.findElement(By.xpath(xPath)).getAttribute("textContent");
		return returnTabName;
	}

	// Change tab
	// Does not check if Tabs exists
	// When changing tab, make sure to wait, as not all elements will be displayed
	// due the fact that it takes time for the javascript to load everything
	public void accessTab(String tabName) {
		String xPath = String.format("//ul[@id='pills-tab']/li/a[contains(text(), '%s')]", tabName);
		driver.findElement(By.xpath(xPath)).click();
	}

	// Get Web ELement for all Tab
	public List<WebElement> retrieveAllTabs() {
		String xPath = "//ul[@id='pills-tab']//li/a";
		List<WebElement> returnList = driver.findElements(By.xpath(xPath));
		return returnList;
	}

	// Table

	// retrieves all table in the page
	public List<DynamicTable> retrieveTables() {
		List<DynamicTable> returnList = new ArrayList<DynamicTable>();

		String inPodXPath = "//section[@class = 'tab-pane fade active show']"
				+ "//div[@class = 'pod']//div[@class = 'bootstrap-table']//ancestor::div[@class = 'pod']";

		List<WebElement> retrievedTables = driver.findElements(By.xpath(inPodXPath));
		if (retrievedTables.size() > 0) {
			System.out.println("Number of Tables: " + retrievedTables.size());
			for (WebElement i : retrievedTables) {
				returnList.add(new DynamicTable(this.driver, i.getAttribute("id")));
			}
		} else {
			try {
				driver.findElement(By.xpath("//div[@class = 'table-responsive']"));
				returnList.add(new DynamicTable(this.driver));
			} catch (NoSuchElementException e) {
				return returnList;
			}
		}
		return returnList;
	}

	// Pod

	// Get the titles of every single pod
	public List<String> getPodTitles() {

		String podTitleXPath = "//section[@class = 'tab-pane fade active show']"
				+ "//div[@class = 'pod']//div[contains(@class,'card-header')]//h4 "
				+ "| //section[@class = 'tab-pane fade active show']"
				// The SVG tag is not a standard HTML tag, hence name() = 'svg' is used
				+ "//*[name() = 'svg']//*[name() = 'text' and @class = 'highcharts-title']//*[name()='tspan']";

		List<String> returnList = new ArrayList<String>();

		List<WebElement> titlesList = driver.findElements(By.xpath(podTitleXPath));
		for (WebElement i : titlesList) {
			returnList.add(i.getAttribute("textContent"));
		}

		return returnList;
	}

}
