package Strikedeck_QA.QA_Automation.framework;

public class FinalEmail {	
	/*
	 * purpose is to read excel report after all test cases have been run
	 * then send complete error messages based on which cases failed for customer
	 * 
	 * theoretically cuts down on spam and gives a clearer understanding on all errors
	 * 		should it be based on CSM or customer?
	 * 		probably one email per CSM with all errors
	 * 		less CSM than customer so it makes sense
	 * 
	 * I just wanted to write this down before i forgot
	 */
}
