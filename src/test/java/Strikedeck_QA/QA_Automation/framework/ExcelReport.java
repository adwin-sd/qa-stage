package Strikedeck_QA.QA_Automation.framework;

import java.io.File;
import java.io.FileInputStream;
//import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.iterators.EmptyListIterator;

/*
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;  
*/

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelReport {
	public static void main(String[] args) throws IOException {
		//setUpExcelReport(); 
		//writeTestCases("test4" , 1, false );		
		//checkColumn("customer_Count");
		sendReportSummary();
	}
	
	//creates rows for excel sheet
	//must run if the excel is blank
	 public static void setUpExcelReport() throws IOException {
		AccountObj.readFile();
		
		// Reading excel sheet
		File source = new File("doc/excelReport.xlsx");
		FileInputStream inputStream = new FileInputStream(source);
		XSSFWorkbook wb = new XSSFWorkbook(inputStream);
		XSSFSheet sheet = wb.getSheet("Report");
		
		//clear sheet
		cleanExcel(sheet);
		
		//populate sheet
		for (int index = 0; index < AccountObj.accountList.size(); index++) {
			if (index == 0) {
				Row row = sheet.createRow(index); // row will change here
				Cell cell = row.createCell(0);
				cell.setCellValue(" ");
			}  
			else if (index > 1){
				Row row = sheet.createRow(index-1); // row will change here
				Cell cell = row.createCell(0); // cell won't change

				String customerName = AccountObj.accountList.get(index).customerName;
				cell.setCellValue(customerName);
			}
		}
		
		// Saves excel
		FileOutputStream fos = new FileOutputStream("doc/excelReport.xlsx");
		wb.write(fos);
		fos.close();
		// System.out.println("EXCEL UPDATED");
		wb.close();
	}
	
	public static void writeTestCases(String testCaseName ,int customerIndex, String testResult) throws IOException {
		//check if testCase exist
		int columnIndex = checkColumn(testCaseName);

		
		// Reading excel sheet
		File source = new File("doc/excelReport.xlsx");
		FileInputStream inputStream = new FileInputStream(source);
		XSSFWorkbook wb = new XSSFWorkbook(inputStream);
		XSSFSheet sheet = wb.getSheet("Report");
		
		//write value to cells
		Row row = sheet.getRow(customerIndex-1); 

		// System.out.println(columnIndex);
		Cell cell = row.createCell(columnIndex);  
		//System.out.printl();
		cell.setCellValue(testResult);
		

		// Saves excel
		FileOutputStream fos = new FileOutputStream("doc/excelReport.xlsx");
		wb.write(fos);
		fos.close();
		// System.out.println("EXCEL UPDATED");
		wb.close();
	}
	
	public static void writeTestCasesInvalid(String testCaseName ,int customerIndex) throws IOException {
		String result = "N/A";
		
		//check if testCase exist
		int columnIndex = checkColumn(testCaseName);

		
		// Reading excel sheet
		File source = new File("doc/excelReport.xlsx");
		FileInputStream inputStream = new FileInputStream(source);
		XSSFWorkbook wb = new XSSFWorkbook(inputStream);
		XSSFSheet sheet = wb.getSheet("Report");
		
		//write value to cells
		Row row = sheet.getRow(customerIndex-1); 

		// System.out.println(columnIndex);
		Cell cell = row.createCell(columnIndex);  
		//System.out.printl();
		cell.setCellValue(result);
		

		// Saves excel
		FileOutputStream fos = new FileOutputStream("doc/excelReport.xlsx");
		wb.write(fos);
		fos.close();
		// System.out.println("EXCEL UPDATED");
		wb.close();
	}
	
	public static int checkColumn(String testCaseName) throws IOException{
		//System.out.println(testCaseName);
		
		//connects to file
		File source = new File("doc/excelReport.xlsx");
		FileInputStream inputStream = new FileInputStream(source);
		XSSFWorkbook wb = new XSSFWorkbook(inputStream);
		XSSFSheet sheet = wb.getSheet("Report");
		
		//gets row 0
		Row headerRow = sheet.getRow(0);
		
		//checks if testCase name exist
		boolean columnExist = false;
		int columnIndex = 1;

		// System.out.println("total number of rows: " + headerRow.getPhysicalNumberOfCells());
		for (int i = 0; i < headerRow.getPhysicalNumberOfCells(); i++ ) {

			Cell cell = headerRow.getCell(i);
			
//			System.out.println(testCaseName);
//			System.out.println(cell.getStringCellValue());
			
			//if testcase name exist
			if (testCaseName.equalsIgnoreCase(cell.getStringCellValue()) ) {
				// System.out.println("column exist");
				//System.out.println(cell.getStringCellValue());
				//return column index
				columnExist = true;
				columnIndex = i;
				break;
			}
		}
		
		if(columnExist == false) {
			// System.out.println("column does not exist");
			//return new column index
			columnIndex = headerRow.getPhysicalNumberOfCells();
			
			//create header
			Cell headerCell = headerRow.createCell(columnIndex);
			headerCell.setCellValue(testCaseName);
			
			//save file
			FileOutputStream fos = new FileOutputStream("doc/excelReport.xlsx");
			wb.write(fos);
			fos.close();
			// System.out.println("column created");
			
		}
		
		//System.out.println(columnIndex);
		wb.close();
		return columnIndex;
		
	}

	public static void cleanExcel(XSSFSheet sheet){

		for (int i = 1; i<=sheet.getLastRowNum(); i++){
			Row row = sheet.getRow(i);
			sheet.removeRow(row);
		}

	}
		
	public static void sendReportSummary() throws IOException {
		AccountObj.readFile();
		//create a list to store list of all test results 
		List<List<String>> completeTestResult = new ArrayList<>();

		//create a list to store each test case name and test result
		List<String> miniTestResult = new ArrayList<>();
		
		//connects to file
		File source = new File("doc/excelReport.xlsx");
		FileInputStream inputStream = new FileInputStream(source);
		XSSFWorkbook wb = new XSSFWorkbook(inputStream);
		XSSFSheet sheet = wb.getSheet("Report");
		
		Row headerRow = sheet.getRow(0); // retrieve row of test case names 

		//remove the -2
		for (int customerIndex = 2; customerIndex < AccountObj.accountList.size(); customerIndex++){
			Row customerRow = sheet.getRow(customerIndex-1); //get row of customer

			//run through test case results
		 	for (int testCaseIndex = 1; testCaseIndex < customerRow.getPhysicalNumberOfCells(); testCaseIndex++ ){
				Cell testCase = headerRow.getCell(testCaseIndex);
				Cell resultCell = customerRow.getCell(testCaseIndex);

				String testCaseName = testCase.getStringCellValue();
				String result = resultCell.getStringCellValue();

				// System.out.println(testCaseName);
				// System.out.println(result);

				//add testCaseName and resultCell into miniTestResult List
				miniTestResult.add(testCaseName);
				miniTestResult.add(result);

				//add test results to main list
				completeTestResult.add(new ArrayList<String>(miniTestResult));
				miniTestResult.clear();
			 }

			 if (completeTestResult.size() == 0){
				String tableBody = "<tr>"
				+"<td colspan='2' style='text-align: center; padding: 3%; font-weight: bold'> Something went wrong. Please check login credentials. </td>"
				+"</tr>";

				Email.email(customerIndex, tableBody, 0);
			 }
			 else{
				 int numberFailedCases = numberFailedCases(completeTestResult);

				 //send report summary
				 String tableBody = Email.tableContent(completeTestResult);
				 Email.email(customerIndex, tableBody, numberFailedCases);
			}
			
			 //clear list for next customer
			 completeTestResult.clear();
			 System.out.println("_________________________________");
		 }

		
		wb.close();
	}

	public static int numberFailedCases(List<List<String>> completeTestResult){
		var numberFailed = 0;

		for (int i = 0; i < completeTestResult.size(); i++){
			if (completeTestResult.get(i).get(1).equals("Fail")){
				numberFailed += 1;
			}
		}
		//System.out.println(numberFailed);
		return numberFailed;
	}

	
	

}