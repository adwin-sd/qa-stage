package Strikedeck_QA.QA_Automation.framework;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class test {
	// This class is for testing structures implemented

	static WebDriverWait wait;
	static WebDriver driver;

	@BeforeTest
	public void setup() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "drivers/chromedriver");
		// driver = Basics.setupHeadlessChrome(driver);
		driver = Basics.setupHeadlessChrome(driver);
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().timeouts().setScriptTimeout(10, TimeUnit.SECONDS);

//		login(driver, "https://blitzdemo1.strikedeck.com/");
		login(driver, "https://sdlitenext.strikedeck.com/");
		Thread.sleep(2000);

		popUpCheck(driver);
		Thread.sleep(1000);
	}

	@AfterTest
	public void endTest() {
		System.out.println("End test");
		driver.quit();
	}

	@Test
	public void testDynamicTable() throws InterruptedException {
		sidebarNavigate(driver, "sidebar_Segment_5cfa812f77659436bf9a6041");
		Thread.sleep(2000);
		System.out.println("Navigated");
		DynamicTable table1 = new DynamicTable(driver);
		System.out.println("Table Initialized");
		int counter = 0;
		int rCounter = 0;
		if (table1.checkDataExist()) {
			List<Map<String, String>> list = table1.retrieveSpecificRowData("Stage", "Growth");
//			List<Map<String, String>> list = table1.retrieveAllRowData();
			for (Map<String, String> i : list) {
				rCounter += 1;
				System.out.println("Row " + rCounter);
				Iterator<String> itr = i.keySet().iterator();
				while (itr.hasNext()) {
					counter += 1;
					String temp = itr.next();
					System.out.println("Data " + counter + ": " + temp + " - " + i.get(temp));
				}
			}
		}

		System.out.println("Table Name: " + table1.getTableName());
		System.out.println("Total No. of Row: " + table1.totalNoOfRows());
		System.out.println(table1.addColumn("CSM"));
		Thread.sleep(5000);
	}

	@Test
	public void testCustomerPage() throws InterruptedException {
		CustomerPage test = new CustomerPage(driver);
		List<String> tiles = test.getTileID();
		System.out.println(tiles.size());
		for (String i : tiles) {
			System.out.println(i);
		}
	}

	@Test
	public void testPage() throws InterruptedException {
		Page page = new Page(driver);

		List<WebElement> tabList = page.retrieveAllTabs();

		int counter = 0;
		for (WebElement i : tabList) {
			counter += 1;
			System.out.println("Tab " + counter + ": " + i.getAttribute("textContent"));
		}

		counter = 0;
		List<DynamicTable> dList = page.retrieveTables();

		for (DynamicTable i : dList) {
			List<String> title = i.retrieveFieldTitles();
			for (String t : title) {
				counter += 1;
				System.out.println("Field " + counter + ": " + t);
			}
			counter = 0;
			System.out.println();
		}

		List<String> pList = page.getPodTitles();
		System.out.println();
		for (String i : pList) {
			System.out.println(i);
		}
		System.out.println();

		page.accessTab("CSM Daily");

		System.out.println(page.currentTabName());
		Thread.sleep(1500);
		List<DynamicTable> oList = page.retrieveTables();

		for (DynamicTable i : oList) {
			System.out.println(i.tableName);
			List<String> title = i.retrieveFieldTitles();
			for (String t : title) {
				counter += 1;
				System.out.println("Field " + counter + ": " + t);
			}
			System.out.println();
			counter = 0;
		}

		sidebarNavigate(driver, "sidebar_customers");

		Thread.sleep(500);

		System.out.println("Customer List");
		Page customerPage = new Page(driver);

		Thread.sleep(1000);
		List<DynamicTable> clList = customerPage.retrieveTables();
		for (DynamicTable i : clList) {
			List<String> title = i.retrieveFieldTitles();
			for (String t : title) {
				counter += 1;
				System.out.println("Field " + counter + ": " + t);
			}
			counter = 0;
		}
	}

	// For testing purposes only, modified from SDBasics
	private static void login(WebDriver wd, String webSite) {
		wait = new WebDriverWait(wd, 10);

		// maximize window
		wd.manage().window().maximize();

		// get instance
		wd.get(webSite);

		// find email, password, login button
		WebElement emailTxt = wait.until(ExpectedConditions.elementToBeClickable(By.id("login-username")));
		WebElement passtxt = wait.until(ExpectedConditions.elementToBeClickable(By.id("login-password")));
		WebElement loginBtn = wait.until(ExpectedConditions.elementToBeClickable(By.className("btn")));

		// retrieve email and password from list
		Scanner myScanner = new Scanner(System.in);

//		System.out.println("Input email in next line: ");
//		String email = myScanner.nextLine();
//		System.out.println("Input password in next line: ");
//		String password = myScanner.nextLine();

		String email = "shabd@strikedeck.com";
		String password = "Strikedeck@123";

		myScanner.close();

		// insert values to fields
		emailTxt.sendKeys(email);
		passtxt.sendKeys(password);

		loginBtn.click();
		System.out.println("Instance Login Successful");
	}

	private static void popUpCheck(WebDriver x) throws InterruptedException {
		Thread.sleep(2000);
		WebElement popUp = wait.until(ExpectedConditions.elementToBeClickable(By.id("popup_welcome")));
		// System.out.print(popUp.getAttribute("style"));
		if (popUp.getAttribute("style").equalsIgnoreCase("display: block;")) {
			WebElement okBtn = wait.until(ExpectedConditions.elementToBeClickable(By.className("btn-muted")));
			okBtn.click();
		}

	}

	@SuppressWarnings("unused")
	private static void sidebarNavigate(WebDriver a, String idName) throws InterruptedException {
		// navigate to idName
		wait = new WebDriverWait(a, 10);
		Thread.sleep(1500);
		WebElement customer = wait.until(ExpectedConditions.elementToBeClickable(By.id(idName)));
		customer.click();
	}

}
