package Strikedeck_QA.QA_Automation.framework;

import java.io.File;

import java.io.FileNotFoundException;
//import java.io.PrintStream;
import java.util.ArrayList;
//import java.util.Formatter;
import java.util.List;
//import java.util.Map;
import java.util.Scanner;



public class ErrorMessages {
	public static String message;
	public static String errorType;
	public static String original = "There is an error for customer: ";
	
	//public static List<String> errType = new ArrayList<>();
	public static List<ErrorMessages> messageList = new ArrayList<>();
	public static String fileName = "doc/errrorMessagesTemplate.csv";


	//hard coded option for getting body message in case of error in customer count
	//replaced by dynamic version -> placeHolderFill()
	public static String countError(int instance, int tile, int given) throws FileNotFoundException {
		AccountObj.readFile();

		//clear message before new iterations
		message = original;

		//get customer name
		String customerName = AccountObj.accountList.get(instance).customerName;
		message += customerName;

		//on new line, give details of error
		message += "\nThe expected customer count is: " + given;
		message += "\nThe actual customer count is: " + tile;

		return message;
	}
	
	public static String placeHolderFill(int errType, String fillValues[]) throws FileNotFoundException{
		messagesReader();
		
		String m = messageList.get(errType).message;
		m = String.format(m, fillValues);
		
		//System.out.println(m);
		
		return m;
	}
	
	
	public static void main(String[] args)  throws FileNotFoundException{
		//for testing purposes only
		
//		ArrayList<String> fillValues = new ArrayList<>(3);
//		fillValues.add("test");
//		fillValues.add("add");
//		fillValues.add("String");
//		fillValues.add("loop");
		
		String[] arr = new String[] {"Hello", "World", "!"};
		String a = "%1$s %2$s%3$s";
		
		
		a = String.format(a, arr);
		
		//expected output: Hello World!
		System.out.println(a);
	}
	

	public static void messagesReader() throws FileNotFoundException{
		//clear list for subsequent iterations
		messageList.clear();

		Scanner scanner = new Scanner(new File(fileName));

		Scanner dataScanner = null;
		int index = 0;

		while (scanner.hasNextLine()) {
			dataScanner = new Scanner(scanner.nextLine());
			dataScanner.useDelimiter(",");
			ErrorMessages m = new ErrorMessages();

			while (dataScanner.hasNext()) {
				String data = dataScanner.next();
				
				if (index == 0)
					m.errorType = data;
				else if (index == 1)
					m.message = data;
				index++;
			}
			index = 0;
			messageList.add(m);
		}
		scanner.close();
	}
}
