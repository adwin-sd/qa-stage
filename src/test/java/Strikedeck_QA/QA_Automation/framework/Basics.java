package Strikedeck_QA.QA_Automation.framework;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

import java.io.FileWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.util.List;
import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;

public class Basics {
	static WebDriver driver;
	//required for reading file of tile ids
	public static List<Basics> idList = new ArrayList<>();
	public static String fileName = "doc/TileID.csv";
	public String name;
	public String id;


	public static Boolean login(WebDriver driver, int count) throws InterruptedException {
		// login and remove popup
		FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(10, TimeUnit.SECONDS)
				.pollingEvery(5, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);
		// maximize window
		// driver.manage().window().maximize();

		// get instance
		String instance = AccountObj.accountList.get(count).instance;
		driver.get(instance);

		// find email, password, login button
		WebElement emailTxt = wait.until(ExpectedConditions.elementToBeClickable(By.id("login-username")));
		WebElement passTxt = wait.until(ExpectedConditions.elementToBeClickable(By.id("login-password")));
		WebElement loginBtn = wait.until(ExpectedConditions.elementToBeClickable(By.className("btn")));

		// retrieve email and password from list and insert into fields
		emailTxt.sendKeys(AccountObj.accountList.get(count).email);
		passTxt.sendKeys(AccountObj.accountList.get(count).password);


		
		Thread.sleep(2000);
		
		loginBtn.click();
		Thread.sleep(2000);
		
		if (driver.getTitle().equals("Strikedeck |")) {
			return false;
		}
		System.out.println("Instance Login Successful");
		driver.switchTo().activeElement();
		
		Thread.sleep(2000);

		WebElement popUp = wait.until(ExpectedConditions.elementToBeClickable(By.id("popup_welcome")));
		// System.out.print(popUp.getAttribute("style"));
		if (popUp.getAttribute("style").equalsIgnoreCase("display: block;")) {
			WebElement okBtn = wait.until(ExpectedConditions.elementToBeClickable(By.className("btn-muted")));
			okBtn.click();
			System.out.println("Pop-up closed");
		}
		return true;

	}

	public static void sidebarNavigate(WebDriver driver, String mainHref, String subId) throws InterruptedException {
		String headerPath = "//a[@href='" + mainHref + "']";

		Thread.sleep(2000);

		// locate main nav btn
		WebElement headerNav = driver.findElement(By.xpath(headerPath));

		// check if main nav is already open
		String header = headerNav.getAttribute("aria-expanded");

		// if header nav is hidden, click to see drop down
		if (header.equalsIgnoreCase("false"))
			headerNav.click();

		Thread.sleep(1000);

		// locate sub nav btn
		WebElement subNav = driver.findElement(By.id(subId));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", subNav);
		Thread.sleep(1000);
		subNav.click();

	}

	// scroll down to particular element in page
	public static void scrollDown(WebDriver driver, String xpath) {
		WebElement element = driver.findElement(By.xpath(xpath));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);

	}

	public static String getTileId(WebDriver driver, String tileName) throws InterruptedException {

		sidebarNavigate(driver, "#parent_Settings", "sidebar_tiles");
		Thread.sleep(1500);

		// filtering table
		WebElement filterBtn = driver.findElement(By.xpath("//*[@id=\"tilesTable\"]/thead/tr/th[1]/div[2]/div/select"));
		filterBtn.click();
		WebElement c360Option = driver
				.findElement(By.xpath("//*[@id=\"tilesTable\"]/thead/tr/th[1]/div[2]/div/select/option[3]"));
		c360Option.click();
		Thread.sleep(2000);

		// set up dynamic table
		DynamicTable tileTable = new DynamicTable(driver);
		List<String> tileList = tileTable.retrieveColumnData("Label01");

		int tileRowIndex = 0;

		// looking for row position in table
		for (int i = 0; i < tileList.size(); i++) {
			if (tileName.equalsIgnoreCase(tileList.get(i))) {
				System.out.println("column exist");
				tileRowIndex = i + 1;
				break;
			}
		}

		// finding element
		WebElement tileTableRow = driver
				.findElement(By.xpath("//*[@id=\"tilesTable\"]/tbody/tr[" + tileRowIndex + "]"));
		Thread.sleep(1000);
		// retrieve tile id
		String tileID = tileTableRow.getAttribute("id");

		System.out.println(tileTableRow.getAttribute("id"));

		sidebarNavigate(driver, "#parent_Customer_360", "sidebar_customer360");

		return tileID;
	}

	public static void getAllTileID(WebDriver driver) throws IOException, InterruptedException {
		//navigate to the table in setting to get necessary data
		Thread.sleep(1500);
		sidebarNavigate(driver, "#parent_Settings", "sidebar_tiles");
		Thread.sleep(1500);

		// filtering table
		WebElement filterBtn = driver.findElement(By.xpath("//*[@id=\"tilesTable\"]/thead/tr/th[1]/div[2]/div/select"));
		filterBtn.click();
		WebElement c360Option = driver
				.findElement(By.xpath("//*[@id=\"tilesTable\"]/thead/tr/th[1]/div[2]/div/select/option[3]"));
		c360Option.click();
		Thread.sleep(2000);

		// set up dynamic table
		DynamicTable tileTable = new DynamicTable(driver);
		List<String> tileList = tileTable.retrieveColumnData("Label01");


		//add all data in csv
		FileWriter csvWriter = new FileWriter("doc/TileID.csv");
		csvWriter.append("Tile Name");
		csvWriter.append(",");
		csvWriter.append("id");
		csvWriter.append(",");
		csvWriter.append("XPath");
		csvWriter.append("\n");

		
		for (int i = 0; i < tileList.size(); i++) {
			csvWriter.append(tileList.get(i));
			csvWriter.append(",");
			WebElement tileTableRow = driver
				.findElement(By.xpath("//*[@id=\"tilesTable\"]/tbody/tr[" + (i+1) + "]"));
			csvWriter.append(tileTableRow.getAttribute("id"));
			csvWriter.append("\n");
		}

		csvWriter.flush();
		csvWriter.close();
		sidebarNavigate(driver, "#parent_Customer_360", "sidebar_customer360");
	}

	public static WebDriver setupChrome(WebDriver driver) {
		System.setProperty("webdriver.chrome.driver", "drivers/chromedriver");
		driver = new ChromeDriver();

		return driver;
	}

	public static WebDriver setupHeadlessChrome(WebDriver driver) {
		System.setProperty("webdriver.chrome.driver", "drivers/chromedriver");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--headless", "--disable-gpu", "--window-size=1920,1200", "--ignore-certificate-errors");
		driver = new ChromeDriver(options);

		return driver;
	}

	public static WebDriver setupHtmlUnit(WebDriver driver) {
		// turn off htmlunit warnings
		java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit").setLevel(java.util.logging.Level.OFF);
		java.util.logging.Logger.getLogger("org.apache.http").setLevel(java.util.logging.Level.OFF);

		driver = new HtmlUnitDriver();

		return driver;
	}
	
	//need to call this for every new instance to update with new tile ids from csv file
	public static void getIDs() throws FileNotFoundException {
		idList.clear();

		Scanner scanner = new Scanner (new File(fileName));
		Scanner dataScanner = null;
		int index = 0;

		while(scanner.hasNextLine()) {
			dataScanner = new Scanner(scanner.nextLine());
			dataScanner.useDelimiter(",");
			Basics info = new Basics();

			while(dataScanner.hasNext()) {
				String data = dataScanner.next();
				
				if (index == 0) {
					info.name = data;
				} else {
					info.id = data;
				}
				index++;
			}
			index = 0;
			idList.add(info);
		}
		scanner.close();

	}

	public static String idSearch (String tileName) {
		for (int i = 0; i < idList.size(); i++) {
			if (idList.get(i).name.equalsIgnoreCase(tileName.toLowerCase())) {
				return idList.get(i).id;
			}
		}
		return "NOT FOUND";
	}

	public static int listSize() {
		return idList.size();
	}
}
