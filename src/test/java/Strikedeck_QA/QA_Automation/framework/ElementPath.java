package Strikedeck_QA.QA_Automation.framework;

import java.io.File;

import java.io.FileNotFoundException;
//import java.io.PrintStream;
import java.util.ArrayList;
//import java.util.Formatter;
import java.util.List;
//import java.util.Map;
import java.util.Scanner;
//import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class ElementPath {
	public String element;
	public String path;

	//static WebDriver driver;

	public static List<ElementPath> pathList = new ArrayList<>();
	public static String fileName = "doc/elementPath.csv";

	public static void pathReader() throws FileNotFoundException {
		//clear list for other iterations
		pathList.clear();

		Scanner scanner = new Scanner(new File(fileName));

		Scanner dataScanner = null;
		int index = 0;

		while(scanner.hasNextLine()) {
			dataScanner = new Scanner(scanner.nextLine());
			dataScanner.useDelimiter(",");
			ElementPath xpth = new ElementPath();

			while (dataScanner.hasNext()) {
				String data = dataScanner.next();

				if (index == 0) {

					xpth.element = data;	
					//System.out.println(data);
				}

				else {
					xpth.path = data;
					//System.out.println(data);
				}

				index++;	

			}
			index = 0;
			pathList.add(xpth);
		}
		scanner.close();
	}

	//usage: WebElement element = ElementPath.getXPath("tileName", driver);
	public static WebElement getXPath(String elementName, WebDriver driver) throws FileNotFoundException{
		pathReader();
		int index = 0;
		String xPath;
		//System.out.println("listsize: " + pathList.size());
		for (int i = 0; i < pathList.size(); i++) {
			if (pathList.get(i).element.equalsIgnoreCase(elementName.toLowerCase())) {
				index = i;
				break;
			}
		}
		
		xPath = pathList.get(index).path;
		//System.out.println(xPath);
		xPath = xPath.substring(1, xPath.length()-1);
		xPath = xPath.replace("\"\"", "\"");
		//xPath = xPath.replace("\\", "");

		//System.out.println(xPath);

		//xPath = "//*[@id=\"tileHelperWithBuilder_9\"]/div/div/p[1]";
		//		String x = "//*[@id=\"tileHelperWithBuilder_1\"]/div/div/p[1]";
		//		System.out.println(x);

		return driver.findElement(By.xpath(xPath));
	}

	public static String getXPath(String elementName) throws FileNotFoundException{
		pathReader();
		int index = 0;
		String xPath;
		for (int i = 0; i < pathList.size(); i++) {
			if (pathList.get(i).element.equalsIgnoreCase(elementName.toLowerCase())) {
				index = i;
				break;
			}
		}
		//System.out.println(pathList.get(index).element);

		xPath = pathList.get(index).path;
		System.out.println(xPath);
		
//		xPath = xPath.replace("\"", "");
		//xPath = xPath.replace("\\", "\"");
//		xPath = xPath.replace("=", "=\"");
//		xPath = xPath.replace("]/", "\"]/");
		
		
		xPath = xPath.substring(1, xPath.length()-1);
		xPath = xPath.replace("\"\"", "\"");
		xPath = xPath.replace("\\", "");
		System.out.println(xPath);
		System.out.println("//*[@id=\"tileHelperWithBuilder_1\"]/div/div/p[1]");


		return xPath;
	}

	public static void main(String[] args) throws FileNotFoundException{
		pathReader();
		System.out.println();
		System.out.println("Customers: ");
		getXPath("customer");
		System.out.println();
		System.out.println("Opportunities");
		getXPath("opportunities");
	}
}
