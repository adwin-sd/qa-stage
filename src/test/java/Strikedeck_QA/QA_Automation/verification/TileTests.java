package Strikedeck_QA.QA_Automation.verification;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import Strikedeck_QA.QA_Automation.framework.AccountObj;
import Strikedeck_QA.QA_Automation.framework.Basics;
import Strikedeck_QA.QA_Automation.framework.ExcelReport;


public class TileTests {
	static WebDriver driver;
	static WebDriverWait wait;
	static String testResult = "PASS";
	static List<String> headers = new ArrayList<>();
	static int customerIndex;

	public static void startTest(int i, int instance) throws InterruptedException {
		String givenVal = AccountObj.accountList.get(instance).dataList.get(i - 5);
		int given = Integer.parseInt(givenVal);
		// get the value for the corresponding tile now

		// get path with tile and replave variable with tile id
		// fix formatting so extraneous quotes are removed3
		Thread.sleep(4000);
		String tileId = Basics.idSearch(headers.get(i));
		String path = AccountObj.accountList.get(1).dataList.get(i - 5);
		path = path.replace("$", "\"");
		path = String.format(path, tileId);

		// find tile and get their value
		WebElement tileValue = driver.findElement(By.xpath(path));
		String tileVal = tileValue.getText();

		// make sure that only numbers are in the string
		// remove anything after a decimal point so that the number will become an int
		String numbersOnly = tileVal.split("\\.", 2)[0];
		numbersOnly = numbersOnly.replaceAll("[^0-9]", "");
		// change String into an int
		int tile = Integer.parseInt(numbersOnly);

		// if the value was a negative int, return the negative back to it
		if (tileVal.indexOf("-") >= 0) {
			tile = tile * (-1);
		}

		System.out.print("Tile Value: " + tile);
		System.out.print("\tGiven Value: " + given + "\t");
		// calculate lower and upper bound values
		// 10% above and below given value
		int bound = (int) (given * .10f) + 1;

		// make sure tile is between upper and lower bounds
		// test for possibility of negative tile value, nm there are neg tile values
		if (((tile > (given + bound) || tile < (given - bound)) && given > 0)
				|| ((tile < (given + bound) || tile > (given - bound)) && given < 0)) {
			testResult = "FAIL";
		}
	}

	public static void testAllTiles(int instance) throws InterruptedException, IOException {

		for (int i = 5; i < headers.size() - 1; i++) {
			System.out.println(headers.get(i).toUpperCase());
			if (!AccountObj.accountList.get(instance).dataList.get(i - 5).equals("")) {
				startTest(i, instance);
				ExcelReport.writeTestCases(headers.get(i), customerIndex, testResult);
			} else {
				testResult = "N/A";
				ExcelReport.writeTestCases(headers.get(i), customerIndex, testResult);
			}
			System.out.println(testResult);
			testResult = "PASS";
		}
	}

	public static void main(String[] args) throws IOException, InterruptedException {
		AccountObj.readFile();
		System.setProperty("webdriver.chrome.driver", "drivers/chromedriver");
		ExcelReport.setUpExcelReport();
		headers = AccountObj.getHeaders();

		for (customerIndex = 2; customerIndex < AccountObj.accountList.size(); customerIndex++) {

			driver = Basics.setupHeadlessChrome(driver);

			System.out.println("Test: Verify Customer Count - " + AccountObj.accountList.get(customerIndex).customerName);

			// login
			if (!Basics.login(driver, customerIndex)) {
				driver.quit();
				continue;
			}

			// call other methods
			// get all tileID values before checking each tile

			Basics.getAllTileID(driver);

			Basics.getIDs();

			testAllTiles(customerIndex);

			testResult = "PASS";

			// exit
			System.out.println("Test Complete");
			driver.quit();
		}
		// once all test cases have been run
		// send an email with the report
		ExcelReport.sendReportSummary();
	}
}
