package Strikedeck_QA.QA_Automation.verification;

// import java.io.File;
import java.io.FileNotFoundException;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
// import org.openqa.selenium.chrome.ChromeDriver;
// import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import Strikedeck_QA.QA_Automation.framework.AccountObj;
import Strikedeck_QA.QA_Automation.framework.Basics;
import Strikedeck_QA.QA_Automation.framework.ErrorMessages;
import Strikedeck_QA.QA_Automation.framework.ExcelReport;



public class CustomerCount {
	static int errID = 1;
	static WebDriver driver;
	static WebDriverWait wait;
	static String testResult = "PASS";
	static String testCaseName = "customer_Count";

	public static void countCheck(int instance) throws FileNotFoundException, InterruptedException {
		// get given customer count from csv file
		System.out.println("Retrieve expected customer count from file");

		// getDataIndex from "customerCount" for test pass testing
		// getDataIndex from "wrongCustomerCount" for test fail testing

		String givenVal = AccountObj.accountList.get(instance).dataList
				.get(AccountObj.getDataIndex("Customers"));

		System.out.println("Given Customer Count: " + givenVal);
		int given = Integer.parseInt(givenVal);

		// figure out how to get this done without thread.sleep()
		Thread.sleep(5000);

		// get customer count from Customer360 customer count tile
		String tileId = Basics.getTileId(driver, "Customers");
		Thread.sleep(2000);
		
		String path = "//p[@onclick=\"window.location='/customers?filter_tile_value01="+ tileId +"'\"]";
		WebElement tileValue = driver.findElement(By.xpath(path));

		String tileVal = tileValue.getText();
		System.out.println("Tile Customer Count: " + tileVal);
		int tile = Integer.parseInt(tileVal);

		// calculate lower and upper bound values
		// 10% above and below given value
		int bound = (int) (given * .10f) + 1;

		//make sure tile is between upper and lower bounds
		//test for possibility of negative tile value
		if (tile > (given + bound) || tile < (given - bound) || tile < 0) {
			testResult = "FAIL";
			//comment out ↓ for running tests w/o sending email
			error(instance, tileVal, givenVal);

		} else {
			System.out.println("Test Passed");
		}
	}

	public static void error(int instance, String tile, String given) throws FileNotFoundException {

		String customer = AccountObj.accountList.get(instance).customerName;
		testResult = "FAIL";

		// the test has failed
		System.out.println("Test Failed. Sending error message to CSM");

		//values that go into place holders of errorMessage
		String[] fillValues = new String[] {customer, tile, given };

		String x = ErrorMessages.placeHolderFill(errID, fillValues);

		// send an email
		//Email.email(instance, x);
	}

	public static void main(String[] args) throws InterruptedException, IOException {
		AccountObj.readFile();
					
		
		//ExcelReport.setUpExcelReport();

		for (int i = 1; i < AccountObj.accountList.size(); i++) {
			
			driver = Basics.setupHeadlessChrome(driver);

			System.out.println("Test: Verify Customer Count");

			// login
			Basics.login(driver, 1);

			// check customer count
			countCheck(i);

			// update Test Cases Report
			ExcelReport.writeTestCases(testCaseName, i, testResult);

			testResult = "PASS";

			// exit
			driver.quit();

		}
	}
}
